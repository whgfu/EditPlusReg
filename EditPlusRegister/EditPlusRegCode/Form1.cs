﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EditPlusRegCode
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Btn_Gen_Click(object sender, EventArgs e)
        {
            TxtRegisterCode.Text = EditPlusCodeGen.GenerateEditplusRegCode(TxtUserName.Text.Trim());

        }

        
    }
}
